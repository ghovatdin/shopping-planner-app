# Einkaufsplaner-App

The project’s main goal is to display the shopping planning with the following information:
name, quantity, due date and it is done or it has pending state.

You can add, update, and remove product from a Product-List.

## Tech Stack

Vue.js, Vuetify.js, HTML, CSS


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/ghovatdin/shopping-planner-app.git
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run serve
```

Run Unit Tests
```
npm run test:unit
```

Compiles and minifies for production
```
npm run build
```

Lints and fixes files
```
npm run lint
```

## Author

- [@ghovatdin](https://gitlab.com/ghovatdin)
