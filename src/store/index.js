import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        // TODO: ProductList soll Daten hier aus Backend bekommen
        productList: [{
            id: "88f202a0-d621-11ed-b226-452f3a1d70b4",
            name: "Laptop",
            count: 3,
            dueDate: "2023-04-13",
            isDone: true,
        },
        {
            id: "88h202a0-d621-11ed-b226-452f3a1d71b6",
            name: "Tablet",
            count: 6,
            dueDate: "2023-04-19",
            isDone: false,
        }]
    },
    mutations: {
        addProduct(state, product) {
            // neues Produkt in dem Array hinzugefügt
            state.productList.unshift(product);
        },
        updateProduct(state, product) {
            // entsprechendes Produkt in dem Array geändert
            state.productList.splice(state.productList.map((item) => item.id).indexOf(product.id), 1, product);
        },
        deleteProduct(state, id) {
            // entsprechendes Produkt in dem Array gelöscht
            state.productList.splice(state.productList.map((item) => item.id).indexOf(id), 1);
        }
    }
})