import { createLocalVue, mount } from '@vue/test-utils'
import Vuetify from 'vuetify';
import AddProduct from "./../../components/dialogs/AddProduct.vue";


describe('AddProduct Test', () => {

    const localVue = createLocalVue();
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify();
    });

    it('Es gibt ein Button', () => {
        const wrapper = mount(AddProduct, { vuetify });
        expect(wrapper.contains('v-btn')).toBe(true)
    })

    it('should add product on button click', () => {

        const wrapper = mount(AddProduct, {
            localVue,
            vuetify,
            propsData: {
                productList: [],
                product: {
                    id: "88h202a0-d621-11ed-b226-452f3a1d71b6",
                    name: "Tablet",
                    count: 6,
                    dueDate: "2023-04-19",
                    isDone: false,
                }
            },
        });

        const event = jest.fn();
        const button = wrapper.find('.v-btn');

        wrapper.vm.$on('action-btn:clicked', event);
        expect(event).toHaveBeenCalledTimes(0);
    });
});
